# luci-app-ncm-status

This package adds a status page to luci. It is based on the version published
at https://sites.google.com/site/variousopenwrt/huawei-e3267 and contains minor
changes to make it work with OpenWrt 18.06.4.

# Usage

Name your wwan connection to `wwan`. Subesquently, set the interface to send
the `AT` commands to interface via uci: `uci set
network.wwan.tty=/dev/ttyUSB0`. After installing this package, the Status menu
should contain an `Ncm status` entry.
